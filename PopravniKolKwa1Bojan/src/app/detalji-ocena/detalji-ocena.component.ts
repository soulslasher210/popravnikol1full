import { Component, OnInit } from '@angular/core';
import { Ocena } from '../ocena';
import { Student } from '../student';
import { Zadatak } from '../zadatak';
import { Router, ActivatedRoute } from '@angular/router';
import { ServisService } from '../servis.service';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-detalji-ocena',
  templateUrl: './detalji-ocena.component.html',
  styleUrls: ['./detalji-ocena.component.css']
})
export class DetaljiOcenaComponent implements OnInit {
  item:Ocena = {}as Ocena;
  items:Zadatak[] = [];
  items2:Student[] = [];
  izmeniForm;

  constructor(private service:ServisService, 
    private router:Router,
    private route:ActivatedRoute,
    private formBuilder:FormBuilder) {
        
    }

  ngOnInit(): void {
    this.dobaviSve();
  }

  dobaviSve(){
    this.service.detaljiOcena(this.route.snapshot.params["id"]).subscribe(data =>{
      this.item = data;

      this.izmeniForm = this.formBuilder.group({
        id:this.item.id,
        studentId:this.item.studentId,
        zadatakId:this.item.zadatakId,
        ocena:this.item.ocena,
      }as Ocena);
    });
    this.service.dobaviZadatak().subscribe(data => this.items = data);
    this.service.dobaviStudent().subscribe(data => this.items2 = data);
  }

  izmeni(item){
    item.studentId = parseInt(item.studentId);
    item.zadatakId = parseInt(item.zadatakId);
    this.service.izmenaOcena(item).subscribe(data => this.item = data);
  }
}
