import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrikazSvihStudentaComponent } from './prikaz-svih-studenta/prikaz-svih-studenta.component';
import { DetaljiStudentComponent } from './detalji-student/detalji-student.component';
import { PrikazSvihZadatakComponent } from './prikaz-svih-zadatak/prikaz-svih-zadatak.component';
import { DetaljiZadatakComponent } from './detalji-zadatak/detalji-zadatak.component';
import { PrikazSvihOcenaComponent } from './prikaz-svih-ocena/prikaz-svih-ocena.component';
import { DetaljiOcenaComponent } from './detalji-ocena/detalji-ocena.component';


const routes: Routes = [
  {path:"prikazStudenta",component:PrikazSvihStudentaComponent},
  {path:"prikazStudenta/prikazDetalja", component:DetaljiStudentComponent},
  {path:"prikazZadatak",component:PrikazSvihZadatakComponent},
  {path:"prikazZadatak/prikazDetalja", component:DetaljiZadatakComponent},
  {path:"prikazOcena",component:PrikazSvihOcenaComponent},
  {path:"prikazOcena/prikazDetalja", component:DetaljiOcenaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
