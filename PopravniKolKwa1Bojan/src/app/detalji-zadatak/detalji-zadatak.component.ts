import { Component, OnInit } from '@angular/core';
import { Zadatak } from '../zadatak';
import { ServisService } from '../servis.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-detalji-zadatak',
  templateUrl: './detalji-zadatak.component.html',
  styleUrls: ['./detalji-zadatak.component.css'],
})
export class DetaljiZadatakComponent implements OnInit {
  item: Zadatak = {} as Zadatak;
  izmeniForm;

  constructor(
    private service: ServisService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.dobaviSve();
  }

  dobaviSve() {
    this.service
      .detaljiZadatak(this.route.snapshot.params['id'])
      .subscribe((data) => {
        this.item = data;
        this.izmeniForm = this.formBuilder.group({
          id: this.item.id,
          oblast: this.item.oblast,
          predmet: this.item.predmet,
        } as Zadatak);
      });
  }

  izmeni(item) {
    this.service.izmenaZadatak(item).subscribe((data) => (this.item = data));
  }
}
