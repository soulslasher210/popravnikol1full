import { Component, OnInit } from '@angular/core';
import { ServisService } from '../servis.service';
import { Student } from '../student';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-prikaz-svih-studenta',
  templateUrl: './prikaz-svih-studenta.component.html',
  styleUrls: ['./prikaz-svih-studenta.component.css']
})
export class PrikazSvihStudentaComponent implements OnInit {

  items:Student[] = [];
  dodajForm;

  constructor(private service:ServisService, 
    private router:Router,
    private formBuilder:FormBuilder) {

        this.dodajForm = this.formBuilder.group({
          id: 0,
          idenks: "",
          ime: "",
          prezime: "",
          datumRodenja: ""
        }as Student);
    }

  ngOnInit(): void {
    this.dobaviSve();
  }

  dobaviSve(){
    this.service.dobaviStudent().subscribe(data => this.items = data);
  }

  obrisi(id){
    this.service.obrisiStudent(id).subscribe(data => this.dobaviSve());
  }

  dodaj(item){
    this.service.dodajStudent(item).subscribe(data => this.dobaviSve());
  }

  detalji(id){
    this.router.navigate(["prikazStudenta/prikazDetalja", {id:id}]);
  }
}
