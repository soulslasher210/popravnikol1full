import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms' ;

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PrikazSvihStudentaComponent } from './prikaz-svih-studenta/prikaz-svih-studenta.component';
import { PrikazSvihOcenaComponent } from './prikaz-svih-ocena/prikaz-svih-ocena.component';
import { PrikazSvihZadatakComponent } from './prikaz-svih-zadatak/prikaz-svih-zadatak.component';
import { DetaljiStudentComponent } from './detalji-student/detalji-student.component';
import { DetaljiZadatakComponent } from './detalji-zadatak/detalji-zadatak.component';
import { DetaljiOcenaComponent } from './detalji-ocena/detalji-ocena.component';

@NgModule({
  declarations: [
    AppComponent,
    PrikazSvihStudentaComponent,
    PrikazSvihOcenaComponent,
    PrikazSvihZadatakComponent,
    DetaljiStudentComponent,
    DetaljiZadatakComponent,
    DetaljiOcenaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
