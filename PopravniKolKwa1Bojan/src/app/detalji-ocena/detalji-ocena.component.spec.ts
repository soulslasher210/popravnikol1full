import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetaljiOcenaComponent } from './detalji-ocena.component';

describe('DetaljiOcenaComponent', () => {
  let component: DetaljiOcenaComponent;
  let fixture: ComponentFixture<DetaljiOcenaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetaljiOcenaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetaljiOcenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
