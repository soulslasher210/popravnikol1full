import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Zadatak } from './zadatak';
import { Ocena } from './ocena';
import { Student } from './student';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServisService {

  adresa:string = "http://localhost:3000/";

  constructor(private http:HttpClient) { }


  dobaviZadatak():Observable<Zadatak[]>{
    return this.http.get<Zadatak[]>(this.adresa + "zadatak");
  }
  obrisiZadatak(id):Observable<Zadatak>{
    return this.http.delete<Zadatak>(this.adresa + "zadatak/" + id);
  }
  dodajZadatak(item):Observable<Zadatak>{
    return this.http.post<Zadatak>(this.adresa + "zadatak", item);
  }
  detaljiZadatak(id):Observable<Zadatak>{
    return this.http.get<Zadatak>(this.adresa + "zadatak/" + id);
  }
  izmenaZadatak(item):Observable<Zadatak>{
    return this.http.put<Zadatak>(this.adresa + "zadatak/" + item.id, item);
  }




  dobaviOcena():Observable<Ocena[]>{
    return this.http.get<Ocena[]>(this.adresa + "ocena");
  }
  obrisiOcena(id):Observable<Ocena>{
    return this.http.delete<Ocena>(this.adresa + "ocena/" + id);
  }
  dodajOcena(item):Observable<Ocena>{
    return this.http.post<Ocena>(this.adresa + "ocena", item);
  }
  detaljiOcena(id):Observable<Ocena>{
    return this.http.get<Ocena>(this.adresa + "ocena/" + id);
  }
  izmenaOcena(item):Observable<Ocena>{
    return this.http.put<Ocena>(this.adresa + "ocena/" + item.id, item);
  }



  dobaviStudent():Observable<Student[]>{
    return this.http.get<Student[]>(this.adresa + "student");
  }
  obrisiStudent(id):Observable<Student>{
    return this.http.delete<Student>(this.adresa + "student/" + id);
  }
  dodajStudent(item):Observable<Student>{
    return this.http.post<Student>(this.adresa + "student", item);
  }
  detaljiStudent(id):Observable<Student>{
    return this.http.get<Student>(this.adresa + "student/" + id);
  }
  izmenaStudent(item):Observable<Student>{
    return this.http.put<Student>(this.adresa + "student/" + item.id, item);
  }

}
