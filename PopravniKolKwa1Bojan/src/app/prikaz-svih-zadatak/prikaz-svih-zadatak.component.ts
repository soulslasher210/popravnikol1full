import { Component, OnInit } from '@angular/core';
import { Zadatak } from '../zadatak';
import { ServisService } from '../servis.service';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-prikaz-svih-zadatak',
  templateUrl: './prikaz-svih-zadatak.component.html',
  styleUrls: ['./prikaz-svih-zadatak.component.css']
})
export class PrikazSvihZadatakComponent implements OnInit {

  items:Zadatak[] = [];
  dodajForm;

  constructor(private service:ServisService, 
    private router:Router,
    private formBuilder:FormBuilder) {

        this.dodajForm = this.formBuilder.group({
          id: 0,
          oblast: "",
          predmet: ""
        }as Zadatak);
    }

  ngOnInit(): void {
    this.dobaviSve();
  }

  dobaviSve(){
    this.service.dobaviZadatak().subscribe(data => this.items = data);
  }

  obrisi(id){
    this.service.obrisiZadatak(id).subscribe(data => this.dobaviSve());
  }

  dodaj(item){
    this.service.dodajZadatak(item).subscribe(data => this.dobaviSve());
  }

  detalji(id){
    this.router.navigate(["prikazZadatak/prikazDetalja", {id:id}]);

  }

}
