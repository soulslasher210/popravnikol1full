import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetaljiZadatakComponent } from './detalji-zadatak.component';

describe('DetaljiZadatakComponent', () => {
  let component: DetaljiZadatakComponent;
  let fixture: ComponentFixture<DetaljiZadatakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetaljiZadatakComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetaljiZadatakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
