import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrikazSvihZadatakComponent } from './prikaz-svih-zadatak.component';

describe('PrikazSvihZadatakComponent', () => {
  let component: PrikazSvihZadatakComponent;
  let fixture: ComponentFixture<PrikazSvihZadatakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrikazSvihZadatakComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrikazSvihZadatakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
