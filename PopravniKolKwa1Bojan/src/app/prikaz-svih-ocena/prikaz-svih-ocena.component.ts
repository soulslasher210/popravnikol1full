import { Component, OnInit } from '@angular/core';
import { Ocena } from '../ocena';
import { Student } from '../student';
import { Zadatak } from '../zadatak';
import { ServisService } from '../servis.service';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-prikaz-svih-ocena',
  templateUrl: './prikaz-svih-ocena.component.html',
  styleUrls: ['./prikaz-svih-ocena.component.css']
})
export class PrikazSvihOcenaComponent implements OnInit {
  items:Ocena[] = [];
  items2:Zadatak[] = [];
  items3:Student[] = [];
  dodajForm;

  constructor(private service:ServisService, 
    private router:Router,
    private formBuilder:FormBuilder) {

        this.dodajForm = this.formBuilder.group({
          id: 0,
          studentId: 0,
          zadatakId: 0,
          ocena: 0,
        }as Ocena);
    }

  ngOnInit(): void {
    this.dobaviSve();
  }

  dobaviSve(){
    this.service.dobaviOcena().subscribe(data => this.items = data);
    this.service.dobaviZadatak().subscribe(data => this.items2 = data);
    this.service.dobaviStudent().subscribe(data => this.items3 = data);
  }

  obrisi(id){
    this.service.obrisiOcena(id).subscribe(data => this.dobaviSve());
  }

  dodaj(item){
    item.studentId = parseInt(item.studentId);
    item.zadatakId = parseInt(item.zadatakId);
    this.service.dodajOcena(item).subscribe(data => this.dobaviSve());
  }

  detalji(id){
    this.router.navigate(["prikazOcena/prikazDetalja", {id:id}]);
  }
}
