import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrikazSvihStudentaComponent } from './prikaz-svih-studenta.component';

describe('PrikazSvihStudentaComponent', () => {
  let component: PrikazSvihStudentaComponent;
  let fixture: ComponentFixture<PrikazSvihStudentaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrikazSvihStudentaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrikazSvihStudentaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
