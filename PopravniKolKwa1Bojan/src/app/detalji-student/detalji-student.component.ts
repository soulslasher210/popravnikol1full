import { Component, OnInit } from '@angular/core';
import { Student } from '../student';
import { ServisService } from '../servis.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Ocena } from '../ocena';

@Component({
  selector: 'app-detalji-student',
  templateUrl: './detalji-student.component.html',
  styleUrls: ['./detalji-student.component.css']
})
export class DetaljiStudentComponent implements OnInit {

  item:Student = {}as Student;
  izmeniForm;

  items2:Ocena[] = [];
  ukupno = [];

  constructor(private service:ServisService, 
    private router:Router,
    private route:ActivatedRoute,
    private formBuilder:FormBuilder) {
        
    }

  ngOnInit(): void {
    this.dobaviSve();
  }

  dobaviSve(){
    this.service.detaljiStudent(this.route.snapshot.params["id"]).subscribe(data =>{
      this.item = data;

      this.izmeniForm = this.formBuilder.group({
        id:this.item.id,
        idenks:this.item.idenks,
        ime:this.item.ime,
        prezime:this.item.prezime,
        datumRodenja:this.item.datumRodenja
      }as Student);
    });
    this.service.dobaviOcena().subscribe(data => this.items2 = data);
  }

  izmeni(item){
    this.service.izmenaStudent(item).subscribe(data => this.item = data);
  }

}
